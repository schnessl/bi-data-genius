import sys
import getopt
import uuid

from sqlalchemy import Table

from utilities.snowflake import create_session_snowflake
from utilities.db import USE_SNOWFLAKE, Permissions, create_connection_sqlite


# usage: python -m admin.add_permission -u <user_id> -p <page>


def add_permission(user_id, page):
    print('adding permission')

    permission_id = str(uuid.uuid4())
    values = {'permission_id': permission_id,
              'user_id': user_id,
              'page': page}

    if USE_SNOWFLAKE:
        try:
            session = create_session_snowflake()

            entry = Permissions(**values)
            session.add(entry)
            session.commit()

            return True
        except Exception as e:
            print(e)

            return False

    else:
        try:
            conn = create_connection_sqlite()

            table = Table(Permissions.__tablename__, Permissions.metadata)
            statement = table.insert().values(**values)
            conn.execute(statement)

            return True
        except Exception as e:
            print(e)

            return False


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 'u:p:')
    except getopt.GetoptError:
        print('usage: python -m admin.add_permission -u <user_id> -p <page>')

        sys.exit(2)

    d_opts = dict(opts)

    user_id = d_opts['-u']
    page = d_opts['-p']

    add_permission(user_id=user_id, page=page)


if __name__ == '__main__':
    main(sys.argv[1:])

import os
import sys
import getopt

from flask import Flask

from utilities.db import db, db_path


# usage: python -m admin.setup -s <snowflake_user> -p <snowflake_pw> -e <email_user> -w <email_pw>


def main(argv):
    try:
        opts, args = getopt.getopt(argv, 's:p:e:w:')
    except getopt.GetoptError:
        print('usage: python -m admin.setup -s <snowflake_user> -p <snowflake_pw> -e <email_user> -w <email_pw>')

        sys.exit(2)

    d_opts = dict(opts)

    snowflake_user = d_opts['-s']
    snowflake_pw = d_opts['-p']
    email_user = d_opts['-e']
    email_pw = d_opts['-w']

    test_str = """SNOWFLAKE_USER={su}\nSNOWFLAKE_PASS={sp}\nEMAIL_USER={eu}\nEMAIL_PASS={ep}""".format(su=snowflake_user,
                                                                                                       sp=snowflake_pw,
                                                                                                       eu=email_user,
                                                                                                       ep=email_pw)

    with open('.env', 'w') as f:
        f.write(test_str)


if __name__ == '__main__':
    server = Flask(__name__)
    server.config.update(
        SECRET_KEY=os.urandom(16),
        SQLALCHEMY_DATABASE_URI=db_path,
        SQLALCHEMY_TRACK_MODIFICATIONS=False
    )

    db.init_app(server)

    server.app_context().push()

    db.create_all()

    print('db created')

    main(sys.argv[1:])

    print('.env created')

import os
import time
import datetime
from functools import lru_cache

import pandas as pd

from sql_queries.data_explorer import USR_MART
from utilities.snowflake import create_engine_snowflake
from utilities.path_definitions import DE_DATA_DIRECTORY
from utilities.file_management import list_files, clear_directory, pandas_read_file


OPERATORS = [['ge ', '>='],
             ['le ', '<='],
             ['lt ', '<'],
             ['gt ', '>'],
             ['ne ', '!='],
             ['eq ', '='],
             ['contains '],
             ['datestartswith ']]


def get_latest_data_date():
    files = list_files(DE_DATA_DIRECTORY)

    if files:
        file_name = files[0]

        data_date_str = file_name.replace('.csv', '')
        data_date = datetime.datetime.strptime(data_date_str, '%Y%m%d').date()

        return data_date
    else:
        return None


def get_usr_data_from_db(file_name, engine=None):
    if engine is None:
        engine = create_engine_snowflake(db='ANALYTICS', schema='PUBLIC')

    print('running usr query')
    usr = pd.read_sql(USR_MART, con=engine)

    file_path = os.path.join(DE_DATA_DIRECTORY, file_name)

    print('clearing directory')
    clear_directory(DE_DATA_DIRECTORY)

    print('writing file')
    usr.to_csv(file_path, index=False)

    return usr


@lru_cache(maxsize=1)
def get_usr_data(engine=None, update=False):
    today = datetime.date.today()

    data_date = get_latest_data_date()

    if data_date:
        print('data found')

        file_name = data_date.strftime('%Y%m%d') + '.csv'
        file_path = os.path.join(DE_DATA_DIRECTORY, file_name)

        if update and data_date != today:
            print('updating data')

            file_name = today.strftime('%Y%m%d') + '.csv'

            usr = get_usr_data_from_db(file_name, engine=engine)
        else:
            usr = pandas_read_file(file_path)

    else:
        print('data not found')

        file_name = today.strftime('%Y%m%d') + '.csv'

        usr = get_usr_data_from_db(file_name, engine=engine)

    time.sleep(0.1)

    return usr


def filter_with_user_list(file_path):
    uploaded = pandas_read_file(file_path, header=None)
    uploaded.rename(columns={0: 'UserID'}, inplace=True)

    usr_list = uploaded['UserID'].unique()

    df = get_usr_data()

    df = df[df['UserID'].isin(usr_list)].copy()

    time.sleep(0.1)

    return df


def split_filter_part(filter_part):
    for operator_type in OPERATORS:
        for operator in operator_type:
            if operator in filter_part:
                name_part, value_part = filter_part.split(operator, 1)
                name = name_part[name_part.find('{') + 1: name_part.rfind('}')]

                value_part = value_part.strip()
                v0 = value_part[0]
                if (v0 == value_part[-1] and v0 in ("'", '"', '`')):
                    value = value_part[1: -1].replace('\\' + v0, v0)
                else:
                    try:
                        value = float(value_part)
                    except ValueError:
                        value = value_part

                # word operators need spaces after them in the filter string,
                # but we don't want these later
                return name, operator_type[0].strip(), value

    return [None] * 3
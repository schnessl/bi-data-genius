import time
import pickle

import numpy as np

from utilities.path_definitions import RELEASE_PERCENTAGES_PATH, CASHOUT_ESTIMATOR_PATH

SIMULATION_ITERATIONS = 1000
CONFIDENCE_INTERVAL_Z = 1.96


def freeround_cashout_estimator(n, fr, release_percentages, cashout_estimators):
    released_n = int(n * release_percentages[fr])

    cashout_mean = cashout_estimators['mean'][fr]
    cashout_std = cashout_estimators['std'][fr]
    cashout_dist_log = np.random.normal(loc=cashout_mean, scale=cashout_std, size=released_n)
    cashout_dist = np.exp(cashout_dist_log)

    cashout_total = np.sum(cashout_dist)

    return cashout_total


def simulate_cashout(n, fr, iterations=SIMULATION_ITERATIONS):
    with open(RELEASE_PERCENTAGES_PATH, 'rb') as f:
        release_percentages = pickle.load(f)
    with open(CASHOUT_ESTIMATOR_PATH, 'rb') as f:
        cashout_estimators = pickle.load(f)

    res = []
    for _ in range(iterations):
        est = freeround_cashout_estimator(n, fr, release_percentages, cashout_estimators)
        res.append(est)

    time.sleep(0.1)

    return res


def calculate_confidence_intervals(data_dict, simulation_iterations=SIMULATION_ITERATIONS):
    res = []

    for fr, data in data_dict.items():
        mean = np.mean(data)
        mean = np.round(mean, 2)
        std = np.std(data)
        # sample_size = np.sqrt(simulation_iterations)

        ci = CONFIDENCE_INTERVAL_Z * std
        ci = np.round(ci, 2)

        res.append({'fr': fr, 'mean': mean, 'ci': ci})

    time.sleep(0.1)

    return res

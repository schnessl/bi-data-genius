import os
import time

import pandas as pd

from utilities.snowflake import create_engine_snowflake
from utilities.path_definitions import EM_TO_USR_RESULT_DIRECTORY
from utilities.file_management import pandas_read_file, pandas_write_file, parse_file_name
from sql_queries.email_to_user import EMAILS_AND_USERS


def process_email_list(file_path, engine=None):
    if engine is None:
        engine = create_engine_snowflake(db='ANALYTICS', schema='PUBLIC')

    email_list = pandas_read_file(file_path, header=None)
    email_list.rename(columns={0: 'Email'}, inplace=True)
    email_list['Email'] = email_list['Email'].apply(str.lower)

    print('running em_to_usr query')
    usr = pd.read_sql(EMAILS_AND_USERS, con=engine)
    print('query completed')
    result = email_list.merge(usr, how='left', on='Email')

    result_name = parse_file_name(file_path)
    result_path = os.path.join(EM_TO_USR_RESULT_DIRECTORY, result_name)

    pandas_write_file(result, result_path, index=False)

    time.sleep(0.1)

    return result_name

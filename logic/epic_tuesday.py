import os
import time

import pandas as pd

from utilities.snowflake import create_engine_snowflake
from utilities.path_definitions import ET_RESULT_DIRECTORY
from utilities.file_management import pandas_read_file, pandas_write_file, parse_file_name
from sql_queries.epic_tuesday import LAST_DEPOSIT_DATE


def write_epic_tuesday_list(file_path, engine=None):
    print('writing et list')
    id_list = pandas_read_file(file_path, header=None)
    id_list.rename(columns={0: 'UserID'}, inplace=True)

    id_list.to_sql(name='epic_tuesdays_users', con=engine, if_exists='replace', index=False)

    time.sleep(0.1)


def process_epic_tuesday(file_path, engine=None):
    if engine is None:
        engine = create_engine_snowflake(db='EPICEROS', schema='DIMENSIONS')

    write_epic_tuesday_list(file_path, engine=engine)

    print('running et query')
    last_deposit = pd.read_sql(LAST_DEPOSIT_DATE, con=engine)
    print('query completed')
    result = last_deposit.groupby(last_deposit.LastDepositDate.dt.year).agg(['count'])

    result_name = parse_file_name(file_path)
    result_path = os.path.join(ET_RESULT_DIRECTORY, result_name)

    pandas_write_file(result, result_path, index=False)

    time.sleep(0.1)

    return result_name

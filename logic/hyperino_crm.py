import os
import time

import pandas as pd

from sql_queries.hyperino_crm import LAST_DEPOSIT_WITHDRAWAL
from utilities.snowflake import create_engine_snowflake
from utilities.path_definitions import HYP_UPLOAD_DIRECTORY, HYP_RESULT_DIRECTORY
from utilities.file_management import pandas_write_file, pandas_read_file, list_files


def write_hyperino_list(id_list, engine=None):
    print('writing hyp list')

    id_list.to_sql(name='hyperino_list', con=engine, if_exists='replace', index=False)

    time.sleep(0.1)


def process_hyperino_crm(files):
    engine = create_engine_snowflake(db='HYPERINO_PROD', schema='DIMENSIONS')

    low = []

    for file_name in files:
        print(file_name)

        file_path = os.path.join(HYP_UPLOAD_DIRECTORY, file_name)

        id_list = pandas_read_file(file_path, skiprows=14)
        write_hyperino_list(id_list, engine=engine)

        print('running hyp query')
        last_deposit = pd.read_sql(LAST_DEPOSIT_WITHDRAWAL, con=engine)

        if 'Low' in file_name:
            low.append(last_deposit)
        else:
            result_path = os.path.join(HYP_RESULT_DIRECTORY, file_name)
            pandas_write_file(last_deposit, result_path)

    low_merge = pd.concat(low)
    result_path = os.path.join(HYP_RESULT_DIRECTORY, 'crm_hyperino_check_low_merged.csv')
    low_merge.to_csv(result_path, index=False)

    time.sleep(0.1)

    return list_files(HYP_RESULT_DIRECTORY)

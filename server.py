import os
from functools import lru_cache

import dash
import dash_bootstrap_components as dbc
from flask import Flask
from flask_login import LoginManager

from utilities.db import USE_SNOWFLAKE, db, db_path, User
from utilities.permissions import fetch_permissions
from utilities.snowflake import create_session_snowflake


server = Flask(__name__)
server.config.update(
    SECRET_KEY=os.urandom(16),
    SQLALCHEMY_DATABASE_URI=db_path,
    SQLALCHEMY_TRACK_MODIFICATIONS=False
)

app = dash.Dash(server=server,
                external_stylesheets=[dbc.themes.BOOTSTRAP],
                suppress_callback_exceptions=True)
app.title = 'BI Data Genius'

db.init_app(server)

login_manager = LoginManager()
login_manager.init_app(server)
login_manager.login_view = '/login'


@login_manager.user_loader
@lru_cache(maxsize=32)
def load_user(user_id):
    print('loading user')
    print(load_user.cache_info())
    if USE_SNOWFLAKE:
        session = create_session_snowflake()
        user = session.query(User).filter_by(user_id=user_id).first()
    else:
        user = User.query.get(user_id)

    if user:
        print('fetching permissions')
        permissions = fetch_permissions(user_id)
        user.permissions = permissions

    return user


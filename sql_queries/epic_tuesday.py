LAST_DEPOSIT_DATE = """
                    SELECT u."UserID",
                                MAX(dep."DepositDate") AS "LastDepositDate"
                         FROM "EPICEROS"."DIMENSIONS"."EPIC_TUESDAYS_USERS" u
                         LEFT JOIN
                           (
                
                            SELECT  pt."UserID",
                                    pt."Completed" AS "DepositDate"
                            FROM "EPICEROS"."RAW_DATA"."HISTORY_DWTRANS" t
                            JOIN "EPICEROS"."RAW_DATA"."HISTORY_DBATRANSACTIONTYPE" tt
                              ON t."Type" = tt."ID"
                             AND tt."TransactionTypeName" = 'Deposit'
                            JOIN "EPICEROS"."RAW_DATA"."HISTORY_DBATRANSACTIONSTATUS" ts
                              ON t."Status" = ts."ID"
                             AND ts."TransactionStatusName" = 'Success'
                            JOIN "EPICEROS"."RAW_DATA"."HISTORY_DWPRETRANS" pt
                              ON t."PreTransID" = pt."ID"
                
                         UNION
                
                            SELECT ROUND(substr(REPLACE(REPLACE(pmt."UserID",'<'),'>'), position('-' in REPLACE(REPLACE(pmt."UserID",'<'),'>'))+1)) as "UserID",
                                  "TransactionCompleted" AS "DepositDate"
                            FROM "EPICEROS"."RAW_DATA"."TRANSACTIONS" pmt
                            WHERE
                                  (
                                     ("TransactionType" = 'Transfer' AND "DebitVendor" = 'TransferWallet')
                                     OR
                                     "TransactionType" = 'Deposit'
                                  )
                            AND "TransactionStatus" = 'Success'
                            AND ROUND(substr(REPLACE(REPLACE(pmt."UserID",'<'),'>'), position('-' in REPLACE(REPLACE(pmt."UserID",'<'),'>'))+1)) IN
                             (
                               SELECT DISTINCT "UserID"
                               FROM "EPICEROS"."DIMENSIONS"."EPIC_TUESDAYS_USERS"
                             )
                
                         UNION
                
                            SELECT ROUND(substr(REPLACE(REPLACE(pmt."UserID",'<'),'>'), position('-' in REPLACE(REPLACE(pmt."UserID",'<'),'>'))+1)) as "UserID",
                                   "TransactionCompleted" AS "DepositDate"
                            FROM "EPICEROS"."ANALYTICS"."EM_MANUAL_TRANSACTION_REPORTING" pmt
                            WHERE "TransactionStatus" = 'Success'
                            AND "PayItemTypeName" = 'DepositCorrections'
                            AND "TransactionType" = 'Vendor2User'
                            AND ROUND(substr(REPLACE(REPLACE(pmt."UserID",'<'),'>'), position('-' in REPLACE(REPLACE(pmt."UserID",'<'),'>'))+1)) IN
                             (
                               SELECT DISTINCT "UserID"
                               FROM "EPICEROS"."DIMENSIONS"."EPIC_TUESDAYS_USERS"
                             )
                           ) dep
                           ON ROUND(dep."UserID") = ROUND(u."UserID")
                           GROUP BY 1;
                    """
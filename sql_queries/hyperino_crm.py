LAST_DEPOSIT_WITHDRAWAL = """
                            WITH hyperino_users AS 
                            (
                              SELECT hl.*, ROUND(u."UserID") AS "UserID"
                              FROM "HYPERINO_PROD"."DIMENSIONS"."HYPERINO_LIST" hl
                              LEFT JOIN "ANALYTICS_PROD"."PUBLIC"."VW_USER" u
                                     ON hl."Username" = u."UserName"
                            )
                            
                            
                            ,deposits_withdrawals AS 
                            (
                            SELECT h."UserID", MAX("DepositDate") AS "LastDepositDate", MAX("WithdrawDate") AS "LastWithdrawalDate"
                            FROM hyperino_users h
                            LEFT JOIN 
                                  (
                                    SELECT DISTINCT ROUND("UserID") AS "UserID",
                                           CASE WHEN "TransactionType" = 'Deposit' AND "Amount" > 0  THEN "TransactionCompleted" END AS "DepositDate",
                                           CASE WHEN "TransactionType" = 'Withdraw' AND "Amount" < 0  THEN "TransactionCompleted" END AS "WithdrawDate"
                                    FROM "HYPERINO_PROD"."ANALYTICS"."TRANSACTION_REPORTING"
                                    WHERE "TransactionStatus" = 'Success'
                                    // AND "TransactionCompleted" <= '2021-04-01'
                                  ) dw
                                 ON dw."UserID" = h."UserID"
                            GROUP BY 1
                            )
                            
                            
                            ,deposits_withdrawals_amount AS 
                            (
                            SELECT h."UserID", SUM("DepositAmount") AS "Last7daysDepositAmount", SUM("WithdrawAmount")*(-1) AS "Last7daysWithdrawalAmount"
                            FROM hyperino_users h
                            LEFT JOIN 
                                  (
                                    SELECT DISTINCT ROUND("UserID") AS "UserID",
                                           CASE WHEN "TransactionType" = 'Deposit' THEN "Amount" END AS "DepositAmount",
                                           CASE WHEN "TransactionType" = 'Withdraw' THEN "Amount" END AS "WithdrawAmount"
                                    FROM "HYPERINO_PROD"."ANALYTICS"."TRANSACTION_REPORTING"
                                    WHERE "TransactionStatus" = 'Success'
                                    AND "TransactionCompleted" > current_date - 7
                                    // AND "TransactionCompleted" between '2021-03-26' and '2021-04-01'
                                  ) dwa
                                 ON dwa."UserID" = h."UserID"
                            GROUP BY 1
                            )
                            
                            
                            SELECT h.*, "LastDepositDate", "LastWithdrawalDate", "Last7daysDepositAmount",  "Last7daysWithdrawalAmount"
                            from hyperino_users h
                            LEFT JOIN deposits_withdrawals dw
                                   ON dw."UserID" = h."UserID"
                            LEFT JOIN deposits_withdrawals_amount dwa
                                   ON dwa."UserID" = h."UserID"       
                          """

# USR_MART = """
#
#             select "UserID",
#                    "Brand",
#                    sum("Deposit_Amount") as "Deposits",
#                    sum("Withdrawal_Amount") as "Withdrawals",
#                    sum(coalesce("Real_money_bets", 0)) as "RMBets",
#                    sum(coalesce("Bonus_money_bets", 0)) as "BMBets",
#                    sum(coalesce("Real_money_win", 0)) as "RMWin",
#                    sum(coalesce("Bonus_money_win", 0)) as "BMWin",
#                    ("RMBets" + "BMBets") - ("RMWin" + "BMWin") as "GGR"
#             from "ANALYTICS_PROD"."PUBLIC"."VW_MGMT_REPORTING2"
#             where "Date" >= '2021-01-01'
#             group by 1, 2;
#
#            """

USR_MART = """

            select *
            from "ANALYTICS_PROD"."PUBLIC"."VW_SIMPLIFY_DATA";
            
"""
# BI Data Genius
Automates silly tasks for business users.

Run the app with `python app.py` from the parent directory.

The SQLite DB and the environment variables (credentials) can be set up with the setup script. Run `python -m admin.setup -s snowuser -p snowpw -e emailuser -w emailpw`.

A script is also provided to manage adding permissions for users. Run `python -m admin.add_permission -u <user_id> -p <page>`.

To add a new page:

 * create a new branch
 * create a file with a `layout()` function in the pages directory
 * add a link in `app.py` header definition (NavBar)
 * add a route in `app.py` router
 * if you have any new requirements recreate `requirements.txt` with `pipreqs --force <path_to_project>`.
 * create a pull request

Next steps:

 * refactor: style could move out to .css files, content copy could move to own file, app could be cleaned up
 * session/connection management could be vastly improved
 * add alerts/feedback on all pages
 * use .arrow files to store data for faster loading
 * model data resources and filter queries for faster performance
 * add tests
 * add more tasks
 * add FAQs
 * create admin page to approve permissions, etc.
 * create configurable page/template engine
 * write script to dump sqlite backend to snowflake
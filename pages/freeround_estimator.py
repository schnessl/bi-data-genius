import time

import plotly.graph_objects as go
import plotly.figure_factory as ff
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output

from server import app
from logic.freeround_estimator import simulate_cashout, calculate_confidence_intervals


PAGE_PREFIX = 'freeround-estimator'


def layout():
    return html.Div(
        [
            html.Br(),
            html.H2('Freeround Cost Estimator'),
            html.P("""Estimate freeround offer cost by adjusting the number of clients the offer will be available for."""),
            html.P("""The graph below will output ranges of cashout estimates for 15, 20, and 30 freerounds.
                      The highest point of each distribution is the most likely cashout value for the offer."""),
            html.Div(
                [
                dcc.Input(id='{}-range-input'.format(PAGE_PREFIX),
                          placeholder='Number of Clients',
                          type='number',
                          min=5,
                          max=100000,
                          step=1,
                          style={'width': '100%'}),
                ]
            ),
            html.H4(id='{}-plot-heading'.format(PAGE_PREFIX)),
            dcc.Loading(
                id='{}-loading-1'.format(PAGE_PREFIX),
                type='default',
                children=dcc.Graph(id='{}-plot'.format(PAGE_PREFIX),
                                   style={'width': '100%',
                                          'height': '500px'}),
            ),
            html.H4(id='{}-result-heading'.format(PAGE_PREFIX)),
            html.Ul(id='{}-result-text'.format(PAGE_PREFIX))
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
    )


@app.callback(
    Output('{}-plot-heading'.format(PAGE_PREFIX), 'children'),
    [Input('{}-range-input'.format(PAGE_PREFIX), 'value')])
def update_plot_heading(n_clients):
    if not n_clients:
        return None
    else:
        return 'Freeround Cashout Estimates: {} Clients Offered'.format(n_clients)


@app.callback(
    Output('{}-plot'.format(PAGE_PREFIX), 'figure'),
    [Input('{}-range-input'.format(PAGE_PREFIX), 'value')])
def render_graph(n_clients):
    if n_clients:
        time.sleep(1)

        fr_values = [30, 20, 15]

        hist_data = [simulate_cashout(n_clients, fr) for fr in fr_values]
        group_labels = ['{}FR'.format(fr) for fr in fr_values]

        fig = ff.create_distplot(hist_data, group_labels)

        fig.update_layout(
            xaxis_title='Cashout (EUR)',
            yaxis_title='Probability Density',
            legend_title='Freerounds',
            margin={'t': 4}
        )

        return fig
    else:
        fig = go.Figure()
        fig.update_layout(
            xaxis={'visible': False},
            yaxis={'visible': False},
            plot_bgcolor='white',
            annotations=[
                {
                    'text': 'Please input number of clients above.',
                    'showarrow': False,
                    'font': {'size': 14}
                }
            ]
        )

    return fig


@app.callback(
    Output('{}-result-heading'.format(PAGE_PREFIX), 'children'),
    Input('{}-plot'.format(PAGE_PREFIX), 'figure'))
def update_result_heading(fig):
    data = fig.get('data')

    if not data:
        return None
    else:
        return '95% Confidence Intervals'


@app.callback(
    Output('{}-result-text'.format(PAGE_PREFIX), 'children'),
    Input('{}-plot'.format(PAGE_PREFIX), 'figure'))
def update_result_text(fig):
    data = fig.get('data')

    if not data:
        return None
    else:
        data_dict = {int(item['legendgroup'].replace('FR', '')): item['x'] for item in data[-1::-1]}
        confidence_intervals = calculate_confidence_intervals(data_dict)

        elems = [html.Li('{fr} Freeround Cashout Cost: €{mean} ± €{ci}'.format(fr=ci['fr'],
                                                                               mean=ci['mean'],
                                                                               ci=ci['ci']))
                 for ci in confidence_intervals]

        return elems

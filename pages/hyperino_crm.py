import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
from flask import send_from_directory

from server import server, app
from logic.hyperino_crm import process_hyperino_crm
from utilities.path_definitions import HYP_UPLOAD_DIRECTORY, HYP_RESULT_DIRECTORY, HYP_EXAMPLE_PATH
from utilities.file_management import save_file, list_files, file_download_link, create_img_src


PAGE_PREFIX = 'hyperino-crm'
img_src = create_img_src(HYP_EXAMPLE_PATH)


def layout():
    return html.Div(
        [
            html.Br(),
            html.H2('CRM Hyperino Deposit & Withdrawal Check'),
            html.P("""Upload user lists in .csv or .xslx format. Any files with the string 'Low' in their names will be
                      merged together. Files should be in the exact format depicted in the example image below."""),
            html.Img(src=img_src,
                     style={'width': '100%'}),
            html.P("""When all files are uploaded press the Submit button and results will be output below.
                      Please wait while loading. Results can take several minutes to retrieve."""),
            dcc.Upload(
                id='{}-upload-data'.format(PAGE_PREFIX),
                children=html.Div(
                    ['Drag and drop or click to select a file to upload.']
                ),
                style={
                    'width': '100%',
                    'height': '60px',
                    'lineHeight': '60px',
                    'borderWidth': '1px',
                    'borderStyle': 'dashed',
                    'borderRadius': '5px',
                    'textAlign': 'center',
                    'margin': '10px',
                },
                multiple=True,
            ),
            html.H4('Uploaded'),
            html.Ul(id='{}-uploaded-files'.format(PAGE_PREFIX)),
            html.Button('Submit',
                        id='{}-submit'.format(PAGE_PREFIX),
                        n_clicks=0,
                        style={'width': '100%',
                               'margin': '10px'}),
            html.H4(id='{}-result-heading'.format(PAGE_PREFIX)),
            dcc.Loading(
                id='{}-loading-1'.format(PAGE_PREFIX),
                type='default',
                children=html.Ul(id='{}-result-files'.format(PAGE_PREFIX)),
            ),
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
)


@server.route('/hyperino_crm/download/<path:path>')
def download_hyperino_crm(path):
    """Serve a file from the result directory."""

    return send_from_directory(HYP_RESULT_DIRECTORY, path, as_attachment=True)


@app.callback(
    Output('{}-uploaded-files'.format(PAGE_PREFIX), 'children'),
    [Input('{}-upload-data'.format(PAGE_PREFIX), 'filename'),
     Input('{}-upload-data'.format(PAGE_PREFIX), 'contents')],
)
def update_uploaded_output(new_uploaded_file_names, new_uploaded_file_contents):
    """Save uploaded files and regenerate the upload list."""

    if new_uploaded_file_names is not None and new_uploaded_file_contents is not None:
        for name, data in zip(new_uploaded_file_names, new_uploaded_file_contents):
            save_file(HYP_UPLOAD_DIRECTORY, name, data)

    files = list_files(HYP_UPLOAD_DIRECTORY)

    if len(files) == 0:
        return 'No files yet!'
    else:
        return [html.Li(f) for f in files]


@app.callback(
    Output('{}-result-heading'.format(PAGE_PREFIX), 'children'),
    Input('{}-submit'.format(PAGE_PREFIX), 'n_clicks'),
)
def update_result_heading_output(n_clicks):
    """Update result heading"""

    if n_clicks == 0:
        return None
    else:
        return 'Result'


@app.callback(
    Output('{}-result-files'.format(PAGE_PREFIX), 'children'),
    Input('{}-submit'.format(PAGE_PREFIX), 'n_clicks'),
)
def update_results_output(n_clicks):
    """Check for uploaded files, execute logic, and regenerate the result list."""

    if n_clicks == 0:
        return None
    else:
        files = list_files(HYP_UPLOAD_DIRECTORY)
        processed_files = process_hyperino_crm(files)

        return [html.Li(file_download_link('hyperino_crm', file_name)) for file_name in processed_files]

import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash import no_update, callback_context
from dash.dependencies import Input, Output, State
from flask_login import login_user
from werkzeug.security import check_password_hash

from server import app
from utilities.auth import retrieve_user
from utilities.audit import add_audit_entry
from utilities.path_definitions import RHINO_LOGO_PATH
from utilities.file_management import create_img_src


PAGE_PREFIX = 'login'

img_src = create_img_src(RHINO_LOGO_PATH)

success_alert = dbc.Alert(
    'Logged in successfully. Taking you home!',
    color='success',
    dismissable=True
)

no_user_alert = dbc.Alert(
    'User does not exist. Please register.',
    color='danger',
    dismissable=True
)

password_incorect_alert = dbc.Alert(
    'Password incorrect. Please try again.',
    color='danger',
    dismissable=True
)


def layout():
    return html.Div(
        [
            dcc.Location(id='{}-url'.format(PAGE_PREFIX),
                         refresh=True,
                         pathname='/login'),
            html.Br(),
            html.H2('Login'),
            html.Img(src=img_src,
                     style={'width': '100%'}),

            html.Div(id='{}-alert'.format(PAGE_PREFIX)),

            dbc.FormGroup(
                    [
                        html.Br(),
                        html.Br(),

                        dbc.Input(id='{}-email'.format(PAGE_PREFIX),
                                  autoFocus=True),
                        dbc.FormText('Email'),

                        html.Br(),
                        dbc.Input(id='{}-password'.format(PAGE_PREFIX),
                                  type='password'),
                        dbc.FormText('Password'),

                        html.Br(),
                        dbc.Button('Submit',
                                   id='{}-button'.format(PAGE_PREFIX),
                                   disabled=True),

                        html.Br(),
                        html.Br(),
                        dcc.Link('Register',
                                 href='/register'),
                        html.Br(),
                        dcc.Link('Forgot Password',
                                 href='/forgot')
                    ]
                )
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
    )


@app.callback(
    Output('{}-button'.format(PAGE_PREFIX), 'disabled'),
    [Input('{}-email'.format(PAGE_PREFIX), 'value'),
     Input('{}-password'.format(PAGE_PREFIX), 'value')]
)
def allow_login_submit(email, password):
    if email and password:
        return False
    else:
        return True


@app.callback(
    [Output('{}-url'.format(PAGE_PREFIX), 'pathname'),
     Output('{}-alert'.format(PAGE_PREFIX), 'children')],
    [Input('{}-button'.format(PAGE_PREFIX), 'n_clicks'),
     Input('{}-password'.format(PAGE_PREFIX), 'value')],
    [State('{}-email'.format(PAGE_PREFIX), 'value')]
)
def login_success(n_clicks, password, email):
    triggered = callback_context.triggered

    if triggered[0]['prop_id'] == '{}-button.n_clicks'.format(PAGE_PREFIX):
        user = retrieve_user(email)

        if user:
            if check_password_hash(user.password, password):
                login_user(user, remember=True)
                add_audit_entry('login')

                return '/home', success_alert
            else:
                return no_update, password_incorect_alert
        else:
            return no_update, no_user_alert
    else:
        return no_update, None

from dash import no_update, callback_context
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc

from server import app
from utilities.auth import retrieve_user, add_user
from utilities.path_definitions import RHINO_LOGO_PATH
from utilities.file_management import create_img_src


PAGE_PREFIX = 'register'

img_src = create_img_src(RHINO_LOGO_PATH)

success_alert = dbc.Alert(
    'Registered successfully. Taking you home!',
    color='success',
    dismissable=True
)

user_exists_alert = dbc.Alert(
    'User already exists. Please login.',
    color='danger',
    dismissable=True
)

unknown_alert = dbc.Alert(
    'Unknown error. Please try again.',
    color='danger',
    dismissable=True
)


def layout():
    return html.Div(
        [
            dcc.Location(id='{}-url'.format(PAGE_PREFIX),
                         refresh=True,
                         pathname='/register'),
            html.Br(),
            html.H2('Register'),
            html.Img(src=img_src,
                     style={'width': '100%'}),

            html.Div(id='{}-alert'.format(PAGE_PREFIX)),

            dbc.FormGroup(
                    [
                        html.Br(),
                        html.Br(),

                        dbc.Input(id='{}-email'.format(PAGE_PREFIX),
                                  autoFocus=True),
                        dbc.FormText('Email'),

                        html.Br(),
                        dbc.Input(id='{}-password'.format(PAGE_PREFIX),
                                  type='password'),
                        dbc.FormText('Password'),

                        html.Br(),
                        dbc.Button('Submit',
                                   id='{}-button'.format(PAGE_PREFIX),
                                   disabled=True),

                        html.Br(),
                        html.Br(),
                        dcc.Link('Login',
                                 href='/login'),
                        html.Br(),
                        dcc.Link('Forgot Password',
                                 href='/forgot')
                    ]
                )
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
    )


@app.callback(
    Output('{}-button'.format(PAGE_PREFIX), 'disabled'),
    [Input('{}-email'.format(PAGE_PREFIX), 'value'),
     Input('{}-password'.format(PAGE_PREFIX), 'value')]
)
def allow_registration_submit(email, password):
    if email and password:
        return False
    else:
        return True


@app.callback(
    [Output('{}-url'.format(PAGE_PREFIX), 'pathname'),
     Output('{}-alert'.format(PAGE_PREFIX), 'children')],
    [Input('{}-button'.format(PAGE_PREFIX), 'n_clicks'),
     Input('{}-email'.format(PAGE_PREFIX), 'value'),
     Input('{}-password'.format(PAGE_PREFIX), 'value')]
)
def registration_success(n_clicks, email, password):
    triggered = callback_context.triggered

    if triggered[0]['prop_id'] == '{}-button.n_clicks'.format(PAGE_PREFIX):
        user = retrieve_user(email)

        if user:
            return no_update, user_exists_alert
        else:
            user_success = add_user(email, password)

            if user_success:
                return '/login', success_alert
            else:
                return no_update, unknown_alert

    else:
        return no_update, None

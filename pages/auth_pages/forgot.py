import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

from server import app
from utilities.path_definitions import RHINO_LOGO_PATH
from utilities.file_management import create_img_src
from utilities.auth import reset_request, retrieve_user


PAGE_PREFIX = 'forgot'

img_src = create_img_src(RHINO_LOGO_PATH)

success_alert = dbc.Alert(
    'Email sent successfully. Please check your inbox.',
    color='success',
    dismissable=True
)

user_not_exists_alert = dbc.Alert(
    'User does not exist. Please register.',
    color='danger',
    dismissable=True
)

unknown_alert = dbc.Alert(
    'Unknown error. Please try again.',
    color='danger',
    dismissable=True
)


def layout():
    return html.Div(
        [
            dcc.Location(id='{}-url'.format(PAGE_PREFIX),
                         refresh=True,
                         pathname='/forgot'),
            html.Br(),
            html.H2('Forgot Password'),
            html.Img(src=img_src,
                     style={'width': '100%'}),

            html.Div(id='{}-alert'.format(PAGE_PREFIX)),

            dbc.FormGroup(
                    [
                        html.Br(),
                        html.Br(),

                        dbc.Input(id='{}-email'.format(PAGE_PREFIX),
                                  autoFocus=True),
                        dbc.FormText('Submit email to receive temporary password'),

                        html.Br(),
                        dbc.Button('Submit',
                                   id='{}-button'.format(PAGE_PREFIX),
                                   disabled=True),

                        html.Br(),
                        html.Br(),
                        dcc.Link('Login',
                                 href='/login'),
                        html.Br(),
                        dcc.Link('Register',
                                 href='/register')
                    ]
                )
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
    )


@app.callback(
    Output('{}-button'.format(PAGE_PREFIX), 'disabled'),
    Input('{}-email'.format(PAGE_PREFIX), 'value')
)
def allow_email_submit(email):
    if email:
        return False
    else:
        return True


@app.callback(
    Output('{}-alert'.format(PAGE_PREFIX), 'children'),
    [Input('{}-email'.format(PAGE_PREFIX), 'value'),
     Input('{}-button'.format(PAGE_PREFIX), 'n_clicks')]
)
def submit_password_reset_request(email, n_clicks):
    triggered = dash.callback_context.triggered

    if triggered[0]['prop_id'] == '{}-button.n_clicks'.format(PAGE_PREFIX):
        user = retrieve_user(email)

        if user:
            reset_request_success = reset_request(email)

            if reset_request_success:
                return success_alert
            else:
                return unknown_alert
        else:
            return user_not_exists_alert
    else:
        return None

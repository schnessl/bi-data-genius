import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
from flask_login import login_user

from server import app
from utilities.audit import add_audit_entry
from utilities.auth import validate_reset_link, retrieve_user


PAGE_PREFIX = 'reset'


def layout():
    return html.Div(
        [
            dcc.Location(id='{}-url'.format(PAGE_PREFIX)),
            html.P('Resetting password...')
        ]
    )


@app.callback(
    Output('{}-url'.format(PAGE_PREFIX), 'pathname'),
    Input('{}-url'.format(PAGE_PREFIX), 'pathname')
)
def attempt_reset(pathname):
    temp_key = pathname.replace('/reset/', '')
    email = validate_reset_link(temp_key)

    if email:
        user = retrieve_user(email)

        login_user(user, remember=True)
        add_audit_entry('password_reset')

        return '/profile'
    else:
        return '/login'

import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
from flask_login import current_user

from server import app
from utilities.auth import change_password
from utilities.email import send_permission_request
from utilities.permissions import POSSIBLE_PERMISSIONS


PAGE_PREFIX = 'profile'

success_alert = dbc.Alert(
    'Password changed successfully.',
    color='success',
    dismissable=True
)

request_alert = dbc.Alert(
    'Permission requested successfully.',
    color='success',
    dismissable=True
)

unknown_alert = dbc.Alert(
    'Unknown error. Please try again.',
    color='danger',
    dismissable=True
)


def layout():
    return html.Div(
        [
            html.Br(),
            html.H2('Profile - {}'.format(current_user.email)),

            dbc.Card(
                [
                    dbc.CardHeader(
                        html.H4(
                            dbc.Button('Change Password',
                                       id='{}-change-password-section-button'.format(PAGE_PREFIX),
                                       color='link')
                        )
                    ),
                    dbc.Collapse(
                        dbc.CardBody(
                            [
                                html.Div(id='{}-password-alert'.format(PAGE_PREFIX)),
                                dbc.FormGroup(
                                    [
                                        html.H4('Change Password'),
                                        html.Br(),

                                        dbc.Input(placeholder='New Password',
                                                  id='{}-new-password'.format(PAGE_PREFIX),
                                                  type='password'),
                                        dbc.FormText('New Password'),

                                        html.Hr(),
                                        html.Br(),

                                        dbc.Button('Save changes',
                                                   color='primary',
                                                   id='{}-password-submit'.format(PAGE_PREFIX),
                                                   disabled=True)
                                    ]
                                )
                            ]
                        ),
                        id='{}-change-password-section-collapse'.format(PAGE_PREFIX)
                    )
                ]
            ),
            dbc.Card(
                [
                    dbc.CardHeader(
                        html.H4(
                            dbc.Button('Request Permissions',
                                       id='{}-request-permissions-section-button'.format(PAGE_PREFIX),
                                       color='link')
                        )
                    ),
                    dbc.Collapse(
                        dcc.Loading(
                            id='{}-loading-1'.format(PAGE_PREFIX),
                            type='default',
                            children=[
                                html.Div(id='{}-request-permissions-alert'.format(PAGE_PREFIX)),
                                dbc.CardBody(id='{}-request-permissions-section-body'.format(PAGE_PREFIX))
                            ]
                        ),
                        id='{}-request-permissions-section-collapse'.format(PAGE_PREFIX)
                    )
                ]
            )
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
    )


@app.callback(
    Output('{}-change-password-section-collapse'.format(PAGE_PREFIX), 'is_open'),
    [Input('{}-change-password-section-collapse'.format(PAGE_PREFIX), 'is_open'),
     Input('{}-change-password-section-button'.format(PAGE_PREFIX), 'n_clicks'),
     Input('{}-request-permissions-section-button'.format(PAGE_PREFIX), 'n_clicks')]
)
def uncollapse_change_password_section(is_open, n_clicks_pw, n_clicks_permission):
    triggered = dash.callback_context.triggered

    if triggered[0]['prop_id'] == '{}-change-password-section-button.n_clicks'.format(PAGE_PREFIX):
        return not is_open
    elif triggered[0]['prop_id'] == '{}-request-permissions-section-button'.format(PAGE_PREFIX):
        return False
    else:
        return False


@app.callback(
    Output('{}-password-submit'.format(PAGE_PREFIX), 'disabled'),
    Input('{}-new-password'.format(PAGE_PREFIX), 'value')
)
def allow_password_submit(new_password):
    if new_password:
        return False
    else:
        return True


@app.callback(
    Output('{}-password-alert'.format(PAGE_PREFIX), 'children'),
    [Input('{}-new-password'.format(PAGE_PREFIX), 'value'),
     Input('{}-password-submit'.format(PAGE_PREFIX), 'n_clicks')]
)
def submit_password_change(new_password, n_clicks):
    triggered = dash.callback_context.triggered

    if triggered[0]['prop_id'] == '{}-password-submit.n_clicks'.format(PAGE_PREFIX):
        change_success = change_password(new_password)

        if change_success:
            return success_alert
        else:
            return unknown_alert

    else:
        return None


@app.callback(
    Output('{}-request-permissions-section-collapse'.format(PAGE_PREFIX), 'is_open'),
    [Input('{}-request-permissions-section-collapse'.format(PAGE_PREFIX), 'is_open'),
     Input('{}-request-permissions-section-button'.format(PAGE_PREFIX), 'n_clicks'),
     Input('{}-change-password-section-button'.format(PAGE_PREFIX), 'n_clicks')]
)
def uncollapse_request_permissions_section(is_open, n_clicks_permission, n_clicks_pw):
    triggered = dash.callback_context.triggered

    if triggered[0]['prop_id'] == '{}-request-permissions-section-button.n_clicks'.format(PAGE_PREFIX):
        return not is_open
    elif triggered[0]['prop_id'] == '{}-change-password-section-button.n_clicks'.format(PAGE_PREFIX):
        return False
    else:
        return False


@app.callback(
    Output('{}-request-permissions-section-body'.format(PAGE_PREFIX), 'children'),
    Input('{}-request-permissions-section-collapse'.format(PAGE_PREFIX), 'is_open')
)
def populate_request_permissions_section(is_open):
    user_permissions = current_user.permissions

    if is_open:
        main_children = []

        for page, description in POSSIBLE_PERMISSIONS.items():
            item_children = [html.H6(page), html.P(description)]

            if page in user_permissions:
                button = html.Button('Access already granted',
                                     id='{}-{}-access-request-button'.format(PAGE_PREFIX, page),
                                     disabled=True,
                                     name=page)
            else:
                button = html.Button('Request access',
                                     id='{}-{}-access-request-button'.format(PAGE_PREFIX, page),
                                     disabled=False,
                                     name=page)

            item_children.append(button)
            item_children.append(html.Hr())

            item_div = html.Div(children=item_children)
            main_children.append(item_div)

        return html.Div(children=main_children)

    else:
        return html.Div()


@app.callback(
    Output('{}-request-permissions-alert'.format(PAGE_PREFIX), 'children'),
    [Input('{}-{}-access-request-button'.format(PAGE_PREFIX, page), 'n_clicks')
     for page in POSSIBLE_PERMISSIONS.keys()]
)
def request_permissions_access(*args):
    triggered = [p['prop_id'] for p in dash.callback_context.triggered if p['value']]

    if triggered:
        page_name = triggered[0].replace('profile-', '').replace('-access-request-button.n_clicks', '')
        email = current_user.email
        user_id = current_user.user_id

        request_success = send_permission_request(email, user_id, page_name)

        if request_success:
            return request_alert
        else:
            return None

    else:
        return None

import dash_html_components as html

from utilities.path_definitions import RHINO_LOGO_PATH
from utilities.file_management import create_img_src


img_src = create_img_src(RHINO_LOGO_PATH)


def layout():
    return html.Div(
        [
            html.Br(),
            html.H2('Welcome to the BI Data Genius'),
            html.Img(src=img_src,
                     style={'width': '100%'}),
            html.P('Please choose a task to complete in the dropdown menu above.'),
            html.P('If you have any questions, please ask the BI team :)',
                   style={'fontStyle': 'italic'}),
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
    )

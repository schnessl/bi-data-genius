import os

import dash
import dash_table
import dash_html_components as html
import dash_core_components as dcc
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output

from server import app
from logic.data_explorer import get_latest_data_date, get_usr_data, filter_with_user_list, split_filter_part
from utilities.path_definitions import DE_UPLOAD_DIRECTORY
from utilities.file_management import save_file, clear_directory


PAGE_PREFIX = 'data-explorer'
DEFAULT_ROW_LIMIT = 1000


def layout():
    return html.Div(
        [
            html.Br(),
            html.H2('Data Explorer'),
            html.P("""Explore user data in the table below. Please wait while loading. Results can take several minutes
                      to retrieve."""),
            html.Div(
                [
                    html.Button('Refresh Data',
                                id='{}-refresh-button'.format(PAGE_PREFIX),
                                n_clicks=0),
                    html.P(id='{}-latest-data-text'.format(PAGE_PREFIX),
                           style={'float': 'right'})
                ]
            ),
            html.Br(),
            html.P("""Upload a user ID list in .csv or .xslx format, and the table will filter to only include
                      uploaded users."""),
            dcc.Upload(
                id='{}-upload-data'.format(PAGE_PREFIX),
                children=html.Div(
                    ['Drag and drop or click to select a file to upload.']
                ),
                style={
                    'width': '100%',
                    'height': '60px',
                    'lineHeight': '60px',
                    'borderWidth': '1px',
                    'borderStyle': 'dashed',
                    'borderRadius': '5px',
                    'textAlign': 'center',
                    'margin': '10px',
                },
                multiple=False,
            ),
            html.P(id='{}-uploaded-file'.format(PAGE_PREFIX)),
            html.P("""Use the slider to adjust the number of rows rendered in the table."""),
            dcc.Slider(
                id='{}-row-number-slider'.format(PAGE_PREFIX),
                min=100,
                max=10000,
                step=100,
                value=1000),
            html.Div(
                [
                    html.P(id='{}-row-number-selected-text'.format(PAGE_PREFIX),
                           style={'float': 'left'}),
                    html.P(id='{}-row-number-available-text'.format(PAGE_PREFIX),
                           style={'float': 'right'})
                ]
            ),
            html.Br(),
            html.Br(),
            html.Div(
                [
                    dcc.Loading(
                        id='{}-loading-1'.format(PAGE_PREFIX),
                        type='default',
                        children=dash_table.DataTable(id='{}-table'.format(PAGE_PREFIX),
                                                      sort_action='native',
                                                      filter_action='custom',
                                                      filter_query='',
                                                      hidden_columns=[],
                                                      export_format='csv',
                                                      style_table={'overflowX': 'auto'},
                                                      page_size=10)
                    )
                ],
                style={'marginLeft': '-25vw',
                       'marginRight': '-25vw'}
            )
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
    )


@app.callback(
    Output('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
    [Input('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
     Input('{}-upload-data'.format(PAGE_PREFIX), 'filename'),
     Input('{}-upload-data'.format(PAGE_PREFIX), 'contents')],
)
def update_uploaded_output(uploaded_file_name, new_uploaded_file_name, new_uploaded_file_contents):
    """Save uploaded files and regenerate the upload list."""

    if new_uploaded_file_name is not None and new_uploaded_file_contents is not None:
        clear_directory(DE_UPLOAD_DIRECTORY)
        save_file(DE_UPLOAD_DIRECTORY, new_uploaded_file_name, new_uploaded_file_contents)

        return new_uploaded_file_name
    elif uploaded_file_name is not None:
        raise PreventUpdate
    else:
        return None


@app.callback(
    Output('{}-latest-data-text'.format(PAGE_PREFIX), 'children'),
    Input('{}-table'.format(PAGE_PREFIX), 'data')
)
def update_latest_data_text(data):
    """Regenerate latest data timestamp."""

    data_date = get_latest_data_date()

    if data_date:
        data_date_str = data_date.strftime('%Y-%m-%d')
        display_str = 'Data updated: {}'.format(data_date_str)

        return display_str
    else:
        return 'No data yet!'


@app.callback(
    Output('{}-row-number-selected-text'.format(PAGE_PREFIX), 'children'),
    Input('{}-row-number-slider'.format(PAGE_PREFIX), 'value')
)
def update_rows_selected_text(row_number):
    """Regenerate rows selected number text."""

    if row_number:
        display_str = 'Rows to display: {}'.format(row_number)

        return display_str
    else:
        return 'No data yet!'


@app.callback(
    [Output('{}-table'.format(PAGE_PREFIX), 'columns'),
     Output('{}-table'.format(PAGE_PREFIX), 'data'),
     Output('{}-table'.format(PAGE_PREFIX), 'tooltip_header'),
     Output('{}-row-number-available-text'.format(PAGE_PREFIX), 'children')],
    [Input('{}-refresh-button'.format(PAGE_PREFIX), 'n_clicks'),
     Input('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
     Input('{}-table'.format(PAGE_PREFIX), 'filter_query'),
     Input('{}-row-number-slider'.format(PAGE_PREFIX), 'value')]
)
def update_data_table(n_clicks, uploaded_file_name, filter, row_selector):
    triggered = dash.callback_context.triggered
    row_limit = row_selector if row_selector else DEFAULT_ROW_LIMIT

    if triggered[0]['prop_id'] == '{}-table.filter_query'.format(PAGE_PREFIX):
        filter = triggered[0]['value']

        filtering_expressions = filter.split(' && ')
        df = get_usr_data(update=False)
        for filter_part in filtering_expressions:
            col_name, operator, filter_value = split_filter_part(filter_part)

            if operator in ('eq', 'ne', 'lt', 'le', 'gt', 'ge'):
                # these operators match pandas series operator method names
                df = df.loc[getattr(df[col_name], operator)(filter_value)]
            elif operator == 'contains':
                df = df.loc[df[col_name].str.contains(filter_value, na=False)]
            elif operator == 'datestartswith':
                # this is a simplification of the front-end filtering logic,
                # only works with complete fields in standard format
                df = df.loc[df[col_name].str.startswith(filter_value)]

        columns = [{'name': i, 'id': i, 'hideable': True} for i in df.columns]
        rows_available_text = 'Rows available: {}'.format(len(df))
        data = df.head(row_limit).to_dict('records')
        tooltip_header = {i: i for i in df.columns}

        return columns, data, tooltip_header, rows_available_text

    if not uploaded_file_name:
        if triggered[0]['prop_id'] == '{}-refresh-button.n_clicks'.format(PAGE_PREFIX):
            df = get_usr_data(update=True)
        else:
            df = get_usr_data(update=False)

        columns = [{'name': i, 'id': i, 'hideable': True} for i in df.columns]
        rows_available_text = 'Rows available: {}'.format(len(df))
        data = df.head(row_limit).to_dict('records')
        tooltip_header = {i: i for i in df.columns}

        return columns, data, tooltip_header, rows_available_text
    else:
        file_path = os.path.join(DE_UPLOAD_DIRECTORY, uploaded_file_name)
        df = filter_with_user_list(file_path)

        columns = [{'name': i, 'id': i, 'hideable': True} for i in df.columns]
        rows_available_text = 'Rows available: {}'.format(len(df))
        data = df.head(row_limit).to_dict('records')
        tooltip_header = {i: i for i in df.columns}

        return columns, data, tooltip_header, rows_available_text

import os

import dash_html_components as html
import dash_core_components as dcc
from dash.exceptions import PreventUpdate
from dash.dependencies import Input, Output
from flask import send_from_directory

from server import server, app
from utilities.path_definitions import ET_UPLOAD_DIRECTORY, ET_RESULT_DIRECTORY, ET_EXAMPLE_PATH
from utilities.file_management import save_file, file_download_link, create_img_src
from logic.epic_tuesday import process_epic_tuesday


PAGE_PREFIX = 'epic-tuesday'
img_src = create_img_src(ET_EXAMPLE_PATH)


def layout():
    return html.Div(
        [
            html.Br(),
            html.H2('Epic Tuesday'),
            html.P("""Upload a user ID list in .csv or .xslx format, and a result will be output below.
                      Files should be in the exact format depicted in the example image below."""),
            html.Img(src=img_src,
                     style={'width': '100%'}),
            html.P("""Please wait while loading. Results can take several minutes to retrieve."""),
            dcc.Upload(
                id='{}-upload-data'.format(PAGE_PREFIX),
                children=html.Div(
                    ['Drag and drop or click to select a file to upload.']
                ),
                style={
                    'width': '100%',
                    'height': '60px',
                    'lineHeight': '60px',
                    'borderWidth': '1px',
                    'borderStyle': 'dashed',
                    'borderRadius': '5px',
                    'textAlign': 'center',
                    'margin': '10px',
                },
                multiple=False,
            ),
            html.H4('Uploaded'),
            html.P(id='{}-uploaded-file-missing-text'.format(PAGE_PREFIX)),
            html.P(id='{}-uploaded-file'.format(PAGE_PREFIX)),
            html.H4(id='{}-result-heading'.format(PAGE_PREFIX)),
            dcc.Loading(
                id='{}-loading-1'.format(PAGE_PREFIX),
                type='default',
                children=html.A(id='{}-result-file'.format(PAGE_PREFIX)),
            ),
        ],
        style={'paddingLeft': '50px',
               'paddingRight': '50px'}
)


@server.route('/epic_tuesday/download/<path:path>')
def download_epic_tuesday(path):
    """Serve a file from the result directory."""

    return send_from_directory(ET_RESULT_DIRECTORY, path, as_attachment=True)


@app.callback(
    Output('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
    [Input('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
     Input('{}-upload-data'.format(PAGE_PREFIX), 'filename'),
     Input('{}-upload-data'.format(PAGE_PREFIX), 'contents')],
)
def update_uploaded_output(uploaded_file_name, new_uploaded_file_name, new_uploaded_file_contents):
    """Save uploaded files and regenerate the upload list."""

    if new_uploaded_file_name is not None and new_uploaded_file_contents is not None:
        save_file(ET_UPLOAD_DIRECTORY, new_uploaded_file_name, new_uploaded_file_contents)
        return new_uploaded_file_name
    elif uploaded_file_name is not None:
        raise PreventUpdate
    else:
        return None


@app.callback(
    Output('{}-uploaded-file-missing-text'.format(PAGE_PREFIX), 'children'),
    Input('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
)
def update_uploaded_missing_output(uploaded_file_name):
    """Check uploaded files and regenerate the upload missing message."""

    if not uploaded_file_name:
        return 'No files yet!'
    else:
        return None


@app.callback(
    Output('{}-result-heading'.format(PAGE_PREFIX), 'children'),
    Input('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
)
def update_result_heading_output(uploaded_file_name):
    """Update result heading"""

    if uploaded_file_name:
        return 'Result'
    else:
        return None


@app.callback(
    Output('{}-result-file'.format(PAGE_PREFIX), 'children'),
    Input('{}-uploaded-file'.format(PAGE_PREFIX), 'children'),
)
def update_results_output(uploaded_file_name):
    """Check for uploaded files, execute logic, and regenerate the result list."""

    if not uploaded_file_name:
        return None
    else:
        file_name = process_epic_tuesday(os.path.join(ET_UPLOAD_DIRECTORY, uploaded_file_name))

        return file_download_link('epic_tuesday', file_name)

from threading import Thread

import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
from flask_login import current_user, logout_user
from flask import session

from server import app
from utilities.audit import add_audit_entry
from utilities.file_management import prep_directories
from utilities.path_definitions import (EM_TO_USR_UPLOAD_DIRECTORY, EM_TO_USR_RESULT_DIRECTORY,
                                        ET_UPLOAD_DIRECTORY, ET_RESULT_DIRECTORY,
                                        HYP_UPLOAD_DIRECTORY, HYP_RESULT_DIRECTORY,
                                        DE_UPLOAD_DIRECTORY, DE_DATA_DIRECTORY)

from pages.auth_pages import login, register, forgot, reset
from pages import home, profile, email_to_user, epic_tuesday, hyperino_crm, freeround_estimator, data_explorer


DROPDOWN_ITEMS = {'email_to_user': 'Email to UserID',
                  'epic_tuesday': 'Epic Tuesday',
                  'hyperino_crm': 'Hyperino CRM Check',
                  'freeround_estimator': 'Freeround Cost Estimator',
                  'data_explorer': 'Data Explorer'}


auth_header = html.Div(
    [
        dbc.NavbarSimple(
            brand='BI Data Genius',
            brand_href='/home',
            color='primary',
            dark=True,
        ),
    ]
)

header = html.Div(
    [
        dbc.NavbarSimple(
            children=[
                dbc.DropdownMenu(
                    nav=True,
                    in_navbar=True,
                    label='Choose a task',
                    id='dropdown_menu'
                ),
                dbc.NavLink('Profile', href='profile'),
                dbc.NavLink('Logout', href='logout'),
            ],
            brand='BI Data Genius',
            brand_href='/home',
            color='primary',
            dark=True,
        ),
    ]
)


def serve_layout():
    """Runs on refresh"""

    layout = html.Div(
        [
            dbc.Container(id='header'),
            html.Div(
                [
                    dbc.Container(
                        id='page-content'
                    )
                ]
            ),
            dcc.Location(id='base-url', refresh=True)
        ],
        style={'margin': 'auto',
               'width': '50%'},
    )

    return layout


app.layout = serve_layout


@app.callback(
    Output('dropdown_menu', 'children'),
    [Input('base-url', 'pathname')]
)
def update_permissioned_dropdown(_):
    if current_user:
        return [dbc.DropdownMenuItem(text, href=page)
                for page, text in DROPDOWN_ITEMS.items()
                if current_user.is_permissioned(page)]
    else:
        return None


@app.callback(
    [Output('header', 'children'),
     Output('page-content', 'children')],
    [Input('base-url', 'pathname')]
)
def router(pathname):
    print(session)
    if current_user.is_authenticated:
        if pathname == '/email_to_user' and current_user.is_permissioned(pathname):
            prep_directories(directories=[EM_TO_USR_UPLOAD_DIRECTORY,
                                          EM_TO_USR_RESULT_DIRECTORY],
                             clear=True)
            thread = Thread(target=add_audit_entry, args=('email_to_user',))
            thread.daemon = True
            thread.start()

            return header, email_to_user.layout()
        elif pathname == '/epic_tuesday' and current_user.is_permissioned(pathname):
            prep_directories(directories=[ET_UPLOAD_DIRECTORY,
                                          ET_RESULT_DIRECTORY],
                             clear=True)
            thread = Thread(target=add_audit_entry, args=('epic_tuesday',))
            thread.daemon = True
            thread.start()

            return header, epic_tuesday.layout()
        elif pathname == '/hyperino_crm' and current_user.is_permissioned(pathname):
            prep_directories(directories=[HYP_UPLOAD_DIRECTORY,
                                          HYP_RESULT_DIRECTORY],
                             clear=True)
            thread = Thread(target=add_audit_entry, args=('hyperino_crm',))
            thread.daemon = True
            thread.start()

            return header, hyperino_crm.layout()
        elif pathname == '/freeround_estimator' and current_user.is_permissioned(pathname):
            thread = Thread(target=add_audit_entry, args=('freeround_estimator',))
            thread.daemon = True
            thread.start()

            return header, freeround_estimator.layout()
        elif pathname == '/data_explorer' and current_user.is_permissioned(pathname):
            prep_directories(directories=[DE_UPLOAD_DIRECTORY],
                             clear=True)
            prep_directories(directories=[DE_DATA_DIRECTORY],
                             clear=False)
            thread = Thread(target=add_audit_entry, args=('data_explorer',))
            thread.daemon = True
            thread.start()

            return header, data_explorer.layout()
        elif pathname == '/profile':
            return header, profile.layout()
        elif pathname == '/logout':
            logout_user()

            return auth_header, login.layout()
        else:
            return header, home.layout()
    else:
        if pathname == '/register':
            return auth_header, register.layout()
        elif pathname == '/forgot':
            return auth_header, forgot.layout()
        elif '/reset' in pathname:
            return auth_header, reset.layout()
        else:
            return auth_header, login.layout()


if __name__ == '__main__':
    app.run_server(debug=True,
                   port=8080,
                   dev_tools_hot_reload=False)

import os
import uuid

from flask_login import UserMixin
from sqlalchemy import create_engine
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base

from utilities.snowflake import construct_snowflake_uri


USE_SNOWFLAKE = True

if USE_SNOWFLAKE:
    db_path = construct_snowflake_uri()
else:
    db_path = 'sqlite:///' + os.path.abspath(os.getcwd()) + '\\app_database.db'

db = SQLAlchemy()
base = declarative_base()


class UserBase(db.Model, base):
    __tablename__ = 'USER'

    if USE_SNOWFLAKE:
        __table_args__ = {'schema': 'ANALYTICS_PROD.DATA_GENIUS_APP'}

    user_id = db.Column(db.String(100), primary_key=True, default=str(uuid.uuid4), unique=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))


class User(UserMixin, UserBase):
    permissions = []

    def get_id(self):
        """Override parent implementation so that id can be called user_id"""
        return self.user_id

    def is_permissioned(self, resource):
        if resource.replace('/', '') in self.permissions:
            return True
        else:
            return False


class TempKeys(db.Model, base):
    __tablename__ = 'TEMP_KEYS'

    if USE_SNOWFLAKE:
        __table_args__ = {'schema': 'ANALYTICS_PROD.DATA_GENIUS_APP'}

    temp_key = db.Column(db.String(100), primary_key=True)
    email = db.Column(db.String(100))
    timestamp = db.Column(db.DateTime())


class Permissions(db.Model, base):
    __tablename__ = 'PERMISSIONS'

    if USE_SNOWFLAKE:
        __table_args__ = {'schema': 'ANALYTICS_PROD.DATA_GENIUS_APP'}

    permission_id = db.Column(db.String(100), primary_key=True, default=str(uuid.uuid4), unique=True)
    user_id = db.Column(db.String(100))
    page = db.Column(db.String(100))


class Audit(db.Model, base):
    __tablename__ = 'AUDIT'

    if USE_SNOWFLAKE:
        __table_args__ = {'schema': 'ANALYTICS_PROD.DATA_GENIUS_APP'}

    action_id = db.Column(db.String(100), primary_key=True, default=str(uuid.uuid4), unique=True)
    user_id = db.Column(db.String(100))
    action = db.Column(db.String(100))
    timestamp = db.Column(db.DateTime())


def create_connection_sqlite():
    engine = create_engine(db_path, connect_args={'check_same_thread': False})
    conn = engine.connect()

    return conn

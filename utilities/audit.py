import uuid
import time
import datetime

from sqlalchemy import Table
from flask_login import current_user

from utilities.db import USE_SNOWFLAKE, Audit, create_connection_sqlite
from utilities.snowflake import create_session_snowflake


AUDIT = True


def add_audit_entry(action):
    if not AUDIT:
        print('skipping audit')
        return False

    print('adding audit entry')

    action_id = str(uuid.uuid4())
    user_id = current_user.user_id
    current_ts = datetime.datetime.now()
    values = {'action_id': action_id,
              'user_id': user_id,
              'action': action,
              'timestamp': current_ts}

    if USE_SNOWFLAKE:
        try:
            session = create_session_snowflake()

            entry = Audit(**values)
            session.add(entry)
            session.commit()

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False

    else:
        try:
            conn = create_connection_sqlite()

            table = Table(Audit.__tablename__, Audit.metadata)
            statement = table.insert().values(**values)
            conn.execute(statement)

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False





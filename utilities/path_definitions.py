import os


ENV_PATH = os.path.join(os.getcwd(), '.env')

EM_TO_USR_UPLOAD_DIRECTORY = os.path.join(os.getcwd(), 'files\\email_to_user\\uploaded')
EM_TO_USR_RESULT_DIRECTORY = os.path.join(os.getcwd(), 'files\\email_to_user\\results')
ET_UPLOAD_DIRECTORY = os.path.join(os.getcwd(), 'files\\epic_tuesday\\uploaded')
ET_RESULT_DIRECTORY = os.path.join(os.getcwd(), 'files\\epic_tuesday\\results')
HYP_UPLOAD_DIRECTORY = os.path.join(os.getcwd(), 'files\\hyperino_crm\\uploaded')
HYP_RESULT_DIRECTORY = os.path.join(os.getcwd(), 'files\\hyperino_crm\\results')
DE_UPLOAD_DIRECTORY = os.path.join(os.getcwd(), 'files\\data_explorer\\uploaded')
DE_DATA_DIRECTORY = os.path.join(os.getcwd(), 'assets\\data\\data_explorer')

RELEASE_PERCENTAGES_PATH = os.path.join(os.getcwd(), 'assets\\models\\freeround_estimator\\freeround_release_percentages.pickle')
CASHOUT_ESTIMATOR_PATH = os.path.join(os.getcwd(), 'assets\\models\\freeround_estimator\\freeround_cashout_estimators.pickle')

RHINO_LOGO_PATH = os.path.join(os.getcwd(), 'assets\\general\\RhinoLogo.png')
EMAIL_TO_USER_EXAMPLE_PATH = os.path.join(os.getcwd(), 'assets\\examples\\email_to_user\\email_to_user_example.png')
ET_EXAMPLE_PATH = os.path.join(os.getcwd(), 'assets\\examples\\epic_tuesday\\epic_tuesday_example.png')
HYP_EXAMPLE_PATH = os.path.join(os.getcwd(), 'assets\\examples\\hyperino_crm\\hyperino_crm_example.png')
import os
import base64
from urllib.parse import quote as urlquote

import pandas as pd
import dash_html_components as html


def save_file(directory, file_name, content):
    """Decode and store an uploaded file."""

    data = content.encode('utf8').split(b';base64,')[1]
    with open(os.path.join(os.getcwd(), directory, file_name), 'wb') as fp:
        fp.write(base64.decodebytes(data))


def list_files(directory):
    """List the files in the directory."""

    files = []
    for file_name in os.listdir(directory):
        file_path = os.path.join(directory, file_name)
        if os.path.isfile(file_path):
            files.append(file_name)
    return files


def clear_directory(directory):
    """Clear all existing files in a directory"""

    files = list_files(directory)
    for file_name in files:
        file_path = os.path.join(directory, file_name)
        os.remove(file_path)


def prep_directories(directories, clear=False):
    """Create directories if missing, delete files inside if necessary"""

    if not isinstance(directories, list):
        directories = [directories]

    for d in directories:
        if not os.path.exists(d):
            os.makedirs(d)
        else:
            if clear:
                clear_directory(d)


def file_download_link(page, file_name):
    """Create a Plotly Dash 'A' element that downloads a file from the app."""

    location = '/{}/download/{}'.format(urlquote(page), urlquote(file_name))
    return html.A(file_name, href=location)


def parse_file_name(file_path, sep='\\'):
    """Splits file path to return file name."""

    file_name = file_path.split(sep)[-1]

    return file_name


def parse_directory_name(file_path, sep='\\'):
    """Splits file path to return directory name."""

    directory_name = file_path.split(sep)[:-1]

    return directory_name


def parse_file_type(file_path, acceptable_types=None):
    """Determines file type of specified file and raises exception if it is not as expected."""

    if not acceptable_types:
        acceptable_types = ['csv', 'xlsx']

    file_type = str.lower(file_path.split('.')[-1])

    if file_type not in [t for t in map(str.lower, acceptable_types)]:
        raise Exception('UnsupportedFileType: {}'.format(file_type))

    return file_type


def pandas_read_file(file_path, *args, **kwargs):
    """Picks appropriate pandas method for specified file and returns data"""

    file_type = parse_file_type(file_path)

    if file_type == 'csv':
        data = pd.read_csv(file_path, *args, **kwargs)
    else:
        data = pd.read_excel(file_path, *args, **kwargs)

    return data


def pandas_write_file(data, result_path, *args, **kwargs):
    """Picks appropriate pandas method for specified filename and writes data"""

    file_name = parse_file_name(result_path)
    file_type = parse_file_type(file_name)

    if file_type == 'csv':
        data.to_csv(result_path, *args, **kwargs)
    else:
        data.to_excel(result_path, *args, **kwargs)

    return file_name


def create_img_src(image_path):
    """Takes image path and returns src string with image data encoded in base64"""

    image_file_type = parse_file_type(image_path, acceptable_types=['jpg', 'png'])
    encoded_image = base64.b64encode(open(image_path, 'rb').read())

    img_src = 'data:image/{};base64, {}'.format(image_file_type, encoded_image.decode('UTF-8'))

    return img_src

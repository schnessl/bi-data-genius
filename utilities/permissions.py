import time

from sqlalchemy import Table

from utilities.db import USE_SNOWFLAKE, Permissions, create_connection_sqlite
from utilities.snowflake import create_session_snowflake


POSSIBLE_PERMISSIONS = {'email_to_user': 'Upload a file with user emails and retrieve their UserIDs.',
                        'data_explorer': 'Explore user data and filter with a file of UserIDs.'}
                        # , 'epic_tuesday': 'Upload a file with Epic Tuesday users and retrieve their last deposit date.',
                        # 'freeround_estimator': 'Estimate the cashout cost of a freeround offer.',
                        # 'hyperino_crm': 'Upload a file with Hyperino users and retrieve their recent deposits and withdrawals.'}


def fetch_permissions(user_id):
    if USE_SNOWFLAKE:
        session = create_session_snowflake()

        res = session.query(Permissions).filter_by(user_id=user_id).all()
    else:
        conn = create_connection_sqlite()

        table = Table(Permissions.__tablename__, Permissions.metadata)
        statement = table.select().where(table.c.user_id == user_id)
        res = conn.execute(statement).fetchall()

    permissions = [p.page for p in res]

    time.sleep(0.1)

    return permissions

import random
import uuid
import time
import datetime

from sqlalchemy import Table, update
from flask_login import current_user
from werkzeug.security import generate_password_hash

from utilities.snowflake import create_session_snowflake
from utilities.email import send_welcome_email, send_reset_email
from utilities.db import USE_SNOWFLAKE, UserBase, User, TempKeys, create_connection_sqlite


def add_user(email, password):
    print('adding user')

    user_id = str(uuid.uuid4())
    hashed_password = generate_password_hash(password, method='sha256')
    values = {'user_id': user_id,
              'email': email,
              'password': hashed_password}

    if USE_SNOWFLAKE:
        try:
            session = create_session_snowflake()

            user = UserBase(**values)
            session.add(user)
            session.commit()

            send_welcome_email(email)

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False

    else:
        try:
            conn = create_connection_sqlite()

            table = Table(UserBase.__tablename__, UserBase.metadata)
            statement = table.insert().values(**values)
            conn.execute(statement)

            send_welcome_email(email)

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False


def retrieve_user(email):
    print('retrieving user')

    if USE_SNOWFLAKE:
        session = create_session_snowflake()

        user = session.query(User).filter_by(email=email).first()

    else:
        user = User.query.filter_by(email=email).first()

    time.sleep(0.1)

    return user


def change_password(new_password):
    print('changing password')

    hashed_password = generate_password_hash(new_password, method='sha256')
    current_user_id = current_user.user_id

    if USE_SNOWFLAKE:
        try:
            session = create_session_snowflake()

            statement = update(UserBase).where(UserBase.user_id == current_user_id).values(password=hashed_password)\
                .execution_options(synchronize_session='fetch')
            session.execute(statement)
            session.commit()

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False

    else:
        try:
            conn = create_connection_sqlite()

            table = Table(UserBase.__tablename__, UserBase.metadata)
            statement = table.update(table).where(table.c.user_id == current_user_id).values(password=hashed_password)
            conn.execute(statement)

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False


def reset_request(email):
    print('resetting password')

    temp_key = ''.join([random.choice('1234567890') for _ in range(50)])
    current_ts = datetime.datetime.now()

    values = {'email': email,
              'temp_key': temp_key,
              'timestamp': current_ts}

    if USE_SNOWFLAKE:
        try:
            session = create_session_snowflake()

            entry = TempKeys(**values)
            session.add(entry)
            session.commit()

            send_reset_email(email, temp_key)

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False

    else:
        try:
            conn = create_connection_sqlite()

            table = Table(TempKeys.__tablename__, TempKeys.metadata)
            statement = table.insert().values(**values)
            conn.execute(statement)

            send_reset_email(email, temp_key)

            time.sleep(0.1)

            return True
        except Exception as e:
            print(e)
            time.sleep(0.1)

            return False


def validate_reset_link(temp_key):
    print('validating reset link')

    current_ts = datetime.datetime.now()

    if USE_SNOWFLAKE:
        session = create_session_snowflake()

        temp_entry = session.query(TempKeys).filter_by(temp_key=temp_key).first()
    else:
        conn = create_connection_sqlite()

        table = Table(TempKeys.__tablename__, TempKeys.metadata)
        statement = table.select().where(table.c.temp_key == temp_key)
        temp_entry = conn.execute(statement).fetchone()

    if temp_entry:
        if temp_entry.timestamp < current_ts - datetime.timedelta(hours=1):

            time.sleep(0.1)

            return None
        else:

            time.sleep(0.1)

            return temp_entry.email
    else:

        time.sleep(0.1)

        return None

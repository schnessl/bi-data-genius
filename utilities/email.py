import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from dotenv import dotenv_values

from utilities.path_definitions import ENV_PATH


def send_email(plain_text, html, subject, recipient, sender_email=None, sender_password=None):
    if not sender_email:
        sender_email = dotenv_values(ENV_PATH)['EMAIL_USER']

    if not sender_password:
        sender_password = dotenv_values(ENV_PATH)['EMAIL_PASS']

    with smtplib.SMTP('smtp.gmail.com', 587) as server:
        server.starttls()
        server.login(sender_email, sender_password)

        msg = MIMEMultipart('alternative')
        plain_mime = MIMEText(plain_text, 'plain')
        html_mime = MIMEText(html, 'html')
        msg.attach(plain_mime)
        msg.attach(html_mime)

        msg['Subject'] = subject

        server.sendmail(sender_email, recipient, msg.as_string())


def send_welcome_email(recipient):
    subject = 'Welcome to the BI Data Genius!'

    plain_text = """Welcome to the BI Data Genius.
                    The next step is to request permission to access certain tasks from the BI team.
                    Please request permission to access the tasks you need through the Profile page on the app."""

    html = """<html>
                <body>
                    <H1>Welcome to the BI Data Genius</H1>
                    <p>The next step is to request permission to access certain tasks from the BI team.
                    <br>
                        Please request permission to access the tasks you need through the Profile page on the app.
                    </p>
                </body>
              </html>"""

    send_email(plain_text, html, subject, recipient)


def send_reset_email(recipient, temp_key):
    subject = 'BI Data Genius - Password Reset Request'

    plain_text = """BI Data Genius - Password Reset Request.
                    Forgot your password? No problem.\n
                    Please go to this link to reset: \n
                    http://127.0.0.1:8080/reset/{} \n
                    Change your password in the profile section immediately upon account recovery.""".format(temp_key)

    html = """<html>
                <body>
                    <H1>BI Data Genius - Password Reset Request</H1>
                    <p>Forgot your password? No problem.
                    <br>
                    Please click <a href="http://127.0.0.1:8080/reset/{}">this link</a> to reset.
                    <br>
                    Change your password in the profile section immediately upon account recovery.
                    </p>
                </body>
              </html>""".format(temp_key)

    send_email(plain_text, html, subject, recipient)


def send_permission_request(email, user_id, page_name, recipient=None):
    if not recipient:
        recipient = dotenv_values(ENV_PATH)['EMAIL_USER']

    subject = 'BI Data Genius - Permission Request'

    plain_text = """BI Data Genius - Permission Request.
                    {} (UserID {}) has requested access to the {} page.\n
                    Please grant their request or inform them they will not be given access.""".format(email,
                                                                                                      user_id,
                                                                                                      page_name)

    html = """<html>
                <body>
                    <H1>BI Data Genius - Permission Request</H1>
                    <p>{} (UserID <b>{}</b>) has requested access to the {} page.
                    <br>
                    Please grant their request or inform them they will not be given access.
                    </p>
                </body>
              </html>""".format(email, user_id, page_name)

    try:
        send_email(plain_text, html, subject, recipient)

        return True
    except Exception as e:
        print(e)

        return False



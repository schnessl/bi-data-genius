import inspect

from dotenv import dotenv_values
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from utilities.path_definitions import ENV_PATH


def construct_snowflake_uri(db='ANALYTICS', schema='DATA_GENIUS_APP', instance='epiceros.eu-central-1',
                            warehouse='MATILLION_ETL', username=None, password=None):
    if not username:
        username = dotenv_values(ENV_PATH)['SNOWFLAKE_USER']

    if not password:
        password = dotenv_values(ENV_PATH)['SNOWFLAKE_PASS']

    uri = 'snowflake://{username}:{password}@{instance}/{db}/{schema}?warehouse={warehouse}&role=SYSADMIN'.format(username=username,
                                                                                                                  password=password,
                                                                                                                  instance=instance,
                                                                                                                  db=db,
                                                                                                                  schema=schema,
                                                                                                                  warehouse=warehouse)

    return uri


def create_engine_snowflake(db='ANALYTICS', schema='DATA_GENIUS_APP', instance='epiceros.eu-central-1',
                            warehouse='MATILLION_ETL', username=None, password=None):

    uri = construct_snowflake_uri(db=db, schema=schema, instance=instance, warehouse=warehouse, username=username,
                                  password=password)
    engine = create_engine(uri)

    return engine


def create_session_snowflake(db='ANALYTICS', schema='DATA_GENIUS_APP', instance='epiceros.eu-central-1',
                             warehouse='MATILLION_ETL', username=None, password=None):

    print('Snowflake session created by: {}'.format(inspect.stack()[1].function))

    engine = create_engine_snowflake(db=db, schema=schema, instance=instance, warehouse=warehouse, username=username,
                                     password=password)
    session = Session(bind=engine)

    return session




